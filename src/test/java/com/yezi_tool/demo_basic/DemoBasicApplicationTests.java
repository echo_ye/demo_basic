package com.yezi_tool.demo_basic;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author Echo_Ye
 * @title
 * @description
 * @date 2020/8/24 11:09
 * @email echo_yezi@qq.com
 */
@SpringBootTest(classes = DemoBasicApplication.class)
class DemoBasicApplicationTests {

    @Test
    void contextLoads() {
    }

}
