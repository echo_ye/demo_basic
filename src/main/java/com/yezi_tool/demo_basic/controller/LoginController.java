package com.yezi_tool.demo_basic.controller;

import com.yezi_tool.demo_basic.commons.model.ReturnMsg;
import lombok.Data;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @author Echo_Ye
 * @title 登录接口
 * @description 用于登录相关接口
 * @date 2020/8/17 9:39
 * @email echo_yezi@qq.com
 */
@Controller
@RequestMapping("/login")
public class LoginController extends BaseController {

    @Data
    private static class LoginRequest {
        private String username;
        private String password;
        private Boolean rememberMe;
    }


    @PostMapping("/login")
    @ResponseBody
    public ReturnMsg login(@RequestBody LoginRequest loginRequest) throws Exception {

        return ReturnMsg.success();
    }

}