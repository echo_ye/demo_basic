package com.yezi_tool.demo_basic.controller;

import org.springframework.stereotype.Controller;

/**
 * @author Echo_Ye
 * @title 基础控制器
 * @description 用于集中常用的公用方法
 * @date 2020/8/24 15:23
 * @email echo_yezi@qq.com
 */
@Controller
public class BaseController {

}
