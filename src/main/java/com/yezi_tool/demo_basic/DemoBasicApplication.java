package com.yezi_tool.demo_basic;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication(scanBasePackages = "com.yezi_tool.demo_basic")
@MapperScan("com.yezi_tool.demo_basic")
@EnableCaching
@EnableScheduling
@EnableTransactionManagement
public class DemoBasicApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoBasicApplication.class, args);
    }

}
