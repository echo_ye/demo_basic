package com.yezi_tool.demo_basic.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yezi_tool.demo_basic.entity.UserInfo;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface UserInfoMapper extends BaseMapper<UserInfo> {
    int updateBatch(List<UserInfo> list);

    int updateBatchSelective(List<UserInfo> list);

    int batchInsert(@Param("list") List<UserInfo> list);
}