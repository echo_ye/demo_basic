package com.yezi_tool.demo_basic.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yezi_tool.demo_basic.entity.TeacherInfo;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface TeacherInfoMapper extends BaseMapper<TeacherInfo> {
    int updateBatch(List<TeacherInfo> list);

    int updateBatchSelective(List<TeacherInfo> list);

    int batchInsert(@Param("list") List<TeacherInfo> list);
}