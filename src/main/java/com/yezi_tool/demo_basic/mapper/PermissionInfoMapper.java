package com.yezi_tool.demo_basic.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yezi_tool.demo_basic.entity.PermissionInfo;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface PermissionInfoMapper extends BaseMapper<PermissionInfo> {
    int updateBatch(List<PermissionInfo> list);

    int updateBatchSelective(List<PermissionInfo> list);

    int batchInsert(@Param("list") List<PermissionInfo> list);

    List<Map<String, Object>> selectByUsername(String username);
}