package com.yezi_tool.demo_basic.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yezi_tool.demo_basic.entity.StudentInfo;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface StudentInfoMapper extends BaseMapper<StudentInfo> {
    int updateBatch(List<StudentInfo> list);

    int updateBatchSelective(List<StudentInfo> list);

    int batchInsert(@Param("list") List<StudentInfo> list);
}