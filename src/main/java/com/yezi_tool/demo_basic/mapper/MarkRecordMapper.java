package com.yezi_tool.demo_basic.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yezi_tool.demo_basic.entity.MarkRecord;

public interface MarkRecordMapper extends BaseMapper<MarkRecord> {
}