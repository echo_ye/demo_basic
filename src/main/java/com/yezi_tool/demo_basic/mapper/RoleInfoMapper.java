package com.yezi_tool.demo_basic.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yezi_tool.demo_basic.entity.RoleInfo;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface RoleInfoMapper extends BaseMapper<RoleInfo> {
    int updateBatch(List<RoleInfo> list);

    int updateBatchSelective(List<RoleInfo> list);

    int batchInsert(@Param("list") List<RoleInfo> list);
}