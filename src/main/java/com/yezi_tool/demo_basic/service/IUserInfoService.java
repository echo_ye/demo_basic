package com.yezi_tool.demo_basic.service;

import com.yezi_tool.demo_basic.entity.UserInfo;

import java.util.List;

/**
 * @author Echo_Ye
 * @title 用户信息server层接口
 * @description 用户信息server层接口
 * @date 2020/8/24 15:20
 * @email echo_yezi@qq.com
 */
public interface IUserInfoService {
    /**
     * 根据id查询
     */
    UserInfo selectById(Long id);

    /**
     * 根据id查询
     */
    UserInfo selectByUserName(String userName);

    /**
     * 查询权限
     */
    List<String> queryPermission(String username);


}
