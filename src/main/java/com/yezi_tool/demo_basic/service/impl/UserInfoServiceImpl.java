package com.yezi_tool.demo_basic.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yezi_tool.demo_basic.entity.UserInfo;
import com.yezi_tool.demo_basic.mapper.PermissionInfoMapper;
import com.yezi_tool.demo_basic.mapper.UserInfoMapper;
import com.yezi_tool.demo_basic.service.IUserInfoService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service("userInfoService")
public class UserInfoServiceImpl implements IUserInfoService {

    private final UserInfoMapper userInfoMapper;
    private final PermissionInfoMapper permissionInfoMapper;

    public UserInfoServiceImpl(UserInfoMapper userInfoMapper, PermissionInfoMapper permissionInfoMapper) {
        this.userInfoMapper = userInfoMapper;
        this.permissionInfoMapper = permissionInfoMapper;
    }

    @Override
    public UserInfo selectById(Long id) {
        return userInfoMapper.selectById(id);
    }

    @Override
    public UserInfo selectByUserName(String userName) {
        return userInfoMapper.selectOne(new QueryWrapper<UserInfo>().eq(UserInfo.COL_USERNAME, userName));
    }

    @Override
    public List<String> queryPermission(String username) {
        List<Map<String, Object>> list = permissionInfoMapper.selectByUsername(username);
        List<String> permissionList = list.size() > 0 ? list.stream().map(m -> String.valueOf(m.get("permissionName"))).collect(Collectors.toList()) : new ArrayList<>();
        return permissionList;
    }
}
