package com.yezi_tool.demo_basic.commons.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserRealmInfo implements Serializable {

    /**
     * 用户名
     */
    private String userName;
    /**
     * 用户id
     */
    private Integer userId;
    /**
     * 角色id
     */
    private Integer roleId;
    /**
     * 权限内容
     */
    private String permission;
}
