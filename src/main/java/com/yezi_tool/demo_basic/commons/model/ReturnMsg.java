package com.yezi_tool.demo_basic.commons.model;

import com.yezi_tool.demo_basic.commons.constants.ConfigConstants;
import com.yezi_tool.demo_basic.commons.constants.ResponseConstants;
import lombok.Data;

import java.io.Serializable;

/**
 * @author Echo_Ye
 * @description 统一返回结果
 * @date 2020/8/6 13:44
 * @email echo_yezi@qq.com
 */
@Data
public class ReturnMsg implements Serializable {

    /**
     * 序列号
     */
    private static final long serialVersionUID = ConfigConstants.SERIAL_VERSION_UID;

    /**
     * 结果
     */
    public static final ReturnMsg SUCCESS = success();//操作成功
    public static final ReturnMsg FAIL = error(ResponseConstants.RETURN_MSG_ERROR);//操作失败
    public static final ReturnMsg UN_LOGIN = error(ResponseConstants.RETURN_MSG_NOT_LOGIN);//未登录

    /**
     * 返回码
     */
    private int code;
    /**
     * 返回信息
     */
    private String msg;
    /**
     * 返回数据
     */
    private Object data;
    /**
     * 是否成功
     */
    private boolean success;

    public ReturnMsg() {
        super();
        this.code = ResponseConstants.RETURN_CODE_SUCCESS;
        this.success = true;
    }

    public ReturnMsg(int code, String msg, Object data, boolean success) {
        super();
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.success = success;
    }

    public static ReturnMsg success() {
        return success(null);
    }

    public static ReturnMsg success(Object data) {
        return new ReturnMsg(ResponseConstants.RETURN_CODE_SUCCESS, ResponseConstants.RETURN_MSG_SUCCESS, data, true);
    }

    public static ReturnMsg error() {
        return error(ResponseConstants.RETURN_MSG_ERROR);
    }

    public static ReturnMsg error(String msg) {
        return error(msg, null);
    }

    public static ReturnMsg error(String msg, Object data) {
        return new ReturnMsg(ResponseConstants.RETURN_CODE_ERROR, msg, data, false);
    }

    public static ReturnMsg unLogin(String msg) {
        return new ReturnMsg(ResponseConstants.RETURN_CODE_NOT_LOGIN, null, msg, false);
    }

}
