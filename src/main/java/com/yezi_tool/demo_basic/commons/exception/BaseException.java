package com.yezi_tool.demo_basic.commons.exception;

import com.yezi_tool.demo_basic.commons.model.ReturnMsg;
/**
 * @author Echo_Ye
 * @description 公共基础异常
 * @date 2020/8/6 11:23
 * @email echo_yezi@qq.com
 */
public class BaseException extends RuntimeException {

    private ReturnMsg returnMsg;

    public BaseException(String msg) throws Exception {
        super(msg);
    }

    public ReturnMsg asReturnMsg() {
        if (null == returnMsg) {
            this.returnMsg = ReturnMsg.error(this.getMessage());
        } else {
            this.returnMsg.setMsg(this.getMessage());
        }
        return this.returnMsg;
    }
}