package com.yezi_tool.demo_basic.commons.utils;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.yezi_tool.demo_basic.commons.constants.CommonConstants;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.*;

/**
 * @author Echo_Ye
 * @description 分页工具
 * @date 2020/8/6 18:05
 * @email echo_yezi@qq.com
 */
public class PageUtil {
    /**
     * 分页参数
     */
    @Data
    @AllArgsConstructor
    public static class PageInfo {
        private long pageSize;
        private long allNum;
        private long allPage;
    }

    /**
     * 分页结果
     */
    @Data
    @AllArgsConstructor
    public static class PageResult {
        private Collection<?> list;
        private PageInfo page;
    }

    /**
     * 获取分页参数
     *
     * @param pageSize 当前页码
     * @param allNum   总数据数量
     * @return 分页参数
     */
    public static Map<String, Object> getPage(int pageSize, int allNum) {
        Map<String, Object> map = Maps.newHashMap();
        int allPage = (allNum + pageSize - 1) / pageSize;
        map.put("pageSize", pageSize);
        map.put("allNum", allNum);
        map.put("allPage", allPage);
        return map;
    }

    /**
     * 对数据进行分页
     *
     * @param data     原数据
     * @param pageSize 当前页码
     * @param pageNum  分页大小
     * @return 分页结果
     */
    public static PageResult fromCollection(List<?> data, Integer pageSize, Integer pageNum) {
        // 计算页数
        pageSize = Optional.ofNullable(pageSize).orElse(CommonConstants.DEFAULT_PAGE_SIZE);
        pageNum = Optional.ofNullable(pageNum).orElse(CommonConstants.DEFAULT_PAGE);
        long allPage = (long) Math.ceil(data.size() / pageSize.doubleValue());
        // 计算页面信息
        PageInfo pageInfo = new PageInfo(pageSize, data.size(), allPage);
        // 内存分页
        List<? extends List<?>> pages = Lists.partition(data, pageSize);
        if (pages.size() >= pageNum) {
            return new PageResult(pages.get(pageNum - 1), pageInfo);
        }
        return new PageResult(Lists.newArrayList(), pageInfo);
    }

    /**
     * 兼容mybatisPlus的结果
     *
     * @param page 通用分页数据
     * @return 分页结果
     */
    public static PageResult fromMyBatisPlus(IPage<?> page) {
        return new PageResult(page.getRecords(), new PageInfo(page.getSize(), page.getTotal(), page.getPages()));
    }

    /**
     * 兼容mybatisPlus的结果
     *
     * @param page         通用分页数据
     * @param dataOverride 实际分页数据
     * @return 分页结果
     */
    public static PageResult fromMyBatisPlus(IPage<?> page, Collection<?> dataOverride) {
        return new PageResult(dataOverride, new PageInfo(page.getSize(), page.getTotal(), page.getPages()));
    }
}
