package com.yezi_tool.demo_basic.commons.utils;

import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.text.SimpleDateFormat;

/**
 * 解析公共类工厂
 * Created by Jhon on 2017/4/7.
 */

public final class ObjectMapperFactory {

    /**
     * 定义一个单例对象
     */
    private static ObjectMapper mapper;

    /**
     * 私有构造
     */
    private ObjectMapperFactory() {
        throw new IllegalAccessError("Utility class");
    }

    /**
     * 懒惰单例模式得到ObjectMapper实例 此对象为Jackson的核心
     *
     * @return 返回结果
     */
    public static synchronized ObjectMapper getInstance() {
        if (mapper == null) {
            mapper = new ObjectMapper();
            //当找不到对应的序列化器时 忽略此字段
            mapper.configure(Feature.ALLOW_SINGLE_QUOTES, true);
            mapper.configure(Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true);
            /**
             * 该特性可以允许接受所有引号引起来的字符，使用‘反斜杠\’机制：如果不允许，只有JSON标准说明书中 列出来的字符可以被避开约束。
             *
             * 由于JSON标准说明中要求为所有控制字符使用引号，这是一个非标准的特性，所以默认是关闭的。
             */
            mapper.configure(Feature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER, true);
            mapper.configure(Feature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER, true);
            mapper.configure(Feature.ALLOW_NUMERIC_LEADING_ZEROS, true);
            mapper.configure(Feature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER, true);
            mapper.configure(Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            mapper.setDateFormat(sdf);
            //使Jackson JSON支持Unicode编码非ASCII字符
//			CustomSerializerFactory serializerFactory = new CustomSerializerFactory();
//			serializerFactory.addSpecificMapping(String.class, new StringUnicodeSerializer());
//			mapper.setSerializerFactory(serializerFactory);
            //支持结束
        }
        return mapper;
    }

}
