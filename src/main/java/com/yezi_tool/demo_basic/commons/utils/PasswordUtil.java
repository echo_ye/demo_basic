package com.yezi_tool.demo_basic.commons.utils;

import com.google.common.base.Strings;
import com.google.common.hash.Hashing;
import org.apache.commons.lang3.RandomStringUtils;


public class PasswordUtil {
    /**
     * 生成盐
     */
    public static String generateSalt() {
        return RandomStringUtils.randomAlphanumeric(16);
    }

    /**
     * 密码加密
     */
    public static String encryptPassword(String password, String salt) {
        return Hashing.hmacMd5(salt.getBytes()).hashBytes(password.getBytes()).toString();
    }

    /**
     * 密码验证
     */
    public static boolean verifyPassword(String password, String salt, String encryptedPassword) {
        return encryptPassword(Strings.nullToEmpty(password), salt).equals(encryptedPassword);
    }
}