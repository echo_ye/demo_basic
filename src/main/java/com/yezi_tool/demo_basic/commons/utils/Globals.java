package com.yezi_tool.demo_basic.commons.utils;

import java.util.HashMap;
import java.util.Map;


/**
 * @author Echo_Ye
 * @title 全局变量定义
 * @description 用于设定全局方法
 * @date 2020/8/17 17:56
 * @email echo_yezi@qq.com
 */
public final class Globals {

    /**
     * 保存全局属性值
     */
    private static Map<String, String> map = new HashMap<String, String>();
    /**
     * 属性文件加载对象
     */
    private static PropertiesLoader propertiesLoader = new PropertiesLoader("application.properties");

    /**
     * 获取配置
     */
    public static String getConfig(String key) {
        String value = map.get(key);
        if (value == null) {
            value = propertiesLoader.getProperty(key);
            map.put(key, value);
        }
        return value;
    }
}
