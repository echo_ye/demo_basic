package com.yezi_tool.demo_basic.commons.constants;

public class TimeConstants {

    /**
     * 时间转换-秒
     */
    public static final long SECONDS_PER_MINUTE = Long.valueOf(60);      //每一分钟
    public static final long SECONDS_PER_HOUR = Long.valueOf(60 * 60);     //每一小时
    public static final long SECONDS_PER_DAY = Long.valueOf(60 * 60 * 24);   //每一天
    public static final long SECONDS_PER_WEEK = Long.valueOf(60 * 60 * 24 * 7);//每一周

    /**
     * 时间转换-毫秒
     */
    public static final long MILLISECONDS_PER_MINUTE    = Long.valueOf(60 * 1000);      //每一分钟
    public static final long MILLISECONDS_PER_HOUR      = Long.valueOf(60 * 60 * 1000);     //每一小时
    public static final long MILLISECONDS_PER_DAY       = Long.valueOf(60 * 60 * 24 * 1000);   //每一天
    public static final long MILLISECONDS_PER_WEEK      = Long.valueOf(60 * 60 * 24 * 7 * 1000);//每一周

    /**
     * 时间格式
     */
    public static final String DATE_FORMAT_HMS = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_FORMAT_ZZZ = "yyyy-MM-dd 00:00:00";
    public static final String DATE_FORMAT_LAST_SECOND = "yyyy-MM-dd 23:59:59";
    public static final String DATE_FORMAT_HM = "yyyy-MM-dd HH:mm";
    public static final String DATE_FORMAT_YMD = "yyyy-MM-dd";
    public static final String DATE_FORMAT_YMD_STRING = "yyyyMMdd";
    public static final String DATE_FORMAT_YYMD = "yy-MM-dd";
    public static final String DATE_FORMAT_YM = "yyyy-MM";
    public static final String DATE_FORMAT_YM_N = "yyyyMM";
    public static final String DATE_FORMAT_Y = "yyyy";
    public static final String DATE_FORMAT_TIME_HMS = "HH:mm:ss";
    public static final String DATE_FORMAT_TIME_HM = "HH:mm";
    public static final String DATE_FORMAT_TIME_YMD_HME = "yyMMddHHmmss";
    public static final String DATE_FORMAT_TIME_YMD_STR = "yyyyMMddHHmmss";
    public static final String DATE_FORMAT_MD = "MM月dd日";
    public static final String DATE_FORMAT_YD = "yyyy年MM月";
}
