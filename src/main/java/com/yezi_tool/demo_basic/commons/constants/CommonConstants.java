package com.yezi_tool.demo_basic.commons.constants;

/**
 * @description 通用常量
 * @author Echo_Ye
 * @date 2020/8/6 14:03
 * @email echo_yezi@qq.com
 */
public class CommonConstants {

    /**
     * 分页相关
     */
    public static final int DEFAULT_PAGE                = 1;    //页码
    public static final int DEFAULT_PAGE_SIZE           = 10;   //分页大小
    public static final int DEFAULT_START_PAGE_INDEX    = 1;    //起始页码

    /**
     * 删除状态
     */
    public static final byte DELETED_N = 0; // 未删除
    public static final byte DELETED_Y = 1; // 已删除

    /**
     * 是否通用
     */
    public static final byte COMMON_YES = 1; //是
    public static final byte COMMON_NO = 0;  //否

    /**
     * 启用状态
     */
    public static final byte STATE_ENABLED = 1;//启用
    public static final byte STATE_DISABLED = 0;//禁用

    /**
     * 性别
     */
    public static final byte GENDER_MALE    = 1; //男
    public static final byte GENDER_FEMALE  = 2; //女
    public static final byte GENDER_UNKNOWN = 0; //未知

    /**
     * 登录类型
     */
    public static final byte LOGIN_MODE_PWD         = 1; //账号密码登录
    public static final byte LOGIN_MODE_MOBILE      = 1; //手机登录

}
