package com.yezi_tool.demo_basic.commons.constants;

public class ResponseConstants {

    /**
     * 统一请求返回码
     */
    public static final int RETURN_CODE_SUCCESS     = 0;    //成功
    public static final int RETURN_CODE_ERROR       = -1;   //错误
    public static final int RETURN_CODE_WARN        = 1;    //警告
    public static final int RETURN_CODE_NOT_LOGIN   = -100;   //未登录

    /**
     * 统一请求返回信息
     */
    public static final String RETURN_MSG_SUCCESS               = "操作成功";
    public static final String RETURN_MSG_ERROR                 = "操作失败";
    public static final String RETURN_MSG_NOT_LOGIN             = "未登录";
    public static final String RETURN_MSG_ABNORMAL_OPERATION    = "操作异常";
    public static final String RETURN_MSG_ABNORMAL_NETWORK      = "网络异常";
    public static final String RETURN_MSG_ABNORMAL_PARAM        = "参数异常";
    public static final String RETURN_MSG_INCORRECT_CAPTCHA     = "验证码错误";

}
