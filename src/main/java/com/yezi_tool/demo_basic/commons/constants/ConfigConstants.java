package com.yezi_tool.demo_basic.commons.constants;

/**
 * @author Echo_Ye
 * @title 配置信息常量
 * @description 用于各种配置
 * @date 2020/8/19 18:13
 * @email echo_yezi@qq.com
 */
public class ConfigConstants {
    /**
     * 序列化相关
     */
    public static final long SERIAL_VERSION_UID     = 1L;    //序列号

    /**
     * redis相关
     */
    public static final String REDIS_KEY_CAPTCHA="captcha";
}
