package com.yezi_tool.demo_basic.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Data;

@Data
@TableName(value = "school_test.student_info")
public class StudentInfo {
    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 姓名
     */
    @TableField(value = "name")
    private String name;

    /**
     * 编号
     */
    @TableField(value = "num")
    private String num;

    /**
     * 性别 1男 2女
     */
    @TableField(value = "gender")
    private Byte gender;

    /**
     * 创建时间
     */
    @TableField(value = "created_time")
    private Date createdTime;

    /**
     * 更细时间（自更新）
     */
    @TableField(value = "updated_time")
    private Date updatedTime;

    /**
     * 年级id
     */
    @TableField(value = "grade_id")
    private Long gradeId;

    /**
     * 手机号
     */
    @TableField(value = "phone")
    private String phone;

    /**
     * 家长手机号
     */
    @TableField(value = "parent_phone")
    private String parentPhone;

    public static final String COL_ID = "id";

    public static final String COL_NAME = "name";

    public static final String COL_NUM = "num";

    public static final String COL_GENDER = "gender";

    public static final String COL_CREATED_TIME = "created_time";

    public static final String COL_UPDATED_TIME = "updated_time";

    public static final String COL_GRADE_ID = "grade_id";

    public static final String COL_PHONE = "phone";

    public static final String COL_PARENT_PHONE = "parent_phone";
}