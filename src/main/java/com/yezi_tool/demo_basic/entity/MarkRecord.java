package com.yezi_tool.demo_basic.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Data;

@Data
@TableName(value = "school_test.mark_record")
public class MarkRecord {
    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 学生id
     */
    @TableField(value = "student_id")
    private Long studentId;

    /**
     * 分值
     */
    @TableField(value = "mark")
    private Long mark;

    /**
     * 学期id
     */
    @TableField(value = "term_id")
    private Long termId;

    /**
     * 科目主键id
     */
    @TableField(value = "subject_id")
    private Long subjectId;

    /**
     * 相关老师id
     */
    @TableField(value = "teacher_id")
    private Long teacherId;

    /**
     * 创建时间
     */
    @TableField(value = "created_time")
    private Date createdTime;

    /**
     * 更新时间
     */
    @TableField(value = "updated_time")
    private Date updatedTime;

    public static final String COL_ID = "id";

    public static final String COL_STUDENT_ID = "student_id";

    public static final String COL_MARK = "mark";

    public static final String COL_TERM_ID = "term_id";

    public static final String COL_SUBJECT_ID = "subject_id";

    public static final String COL_TEACHER_ID = "teacher_id";

    public static final String COL_CREATED_TIME = "created_time";

    public static final String COL_UPDATED_TIME = "updated_time";
}