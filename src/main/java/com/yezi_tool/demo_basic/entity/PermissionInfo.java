package com.yezi_tool.demo_basic.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@TableName(value = "school_test.permission_info")
public class PermissionInfo {
    public static final String COL_PERMISSIONNAME = "permissionname";
    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 权限名
     */
    @TableField(value = "permission_name")
    private String permissionName;

    /**
     * 外键关联role
     */
    @TableField(value = "role_id")
    private Integer roleId;

    public static final String COL_ID = "id";

    public static final String COL_PERMISSION_NAME = "permission_name";

    public static final String COL_ROLE_ID = "role_id";
}