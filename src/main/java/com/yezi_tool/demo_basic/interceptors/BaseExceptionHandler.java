package com.yezi_tool.demo_basic.interceptors;

import com.yezi_tool.demo_basic.commons.exception.BaseException;
import com.yezi_tool.demo_basic.commons.model.ReturnMsg;
import com.yezi_tool.demo_basic.commons.utils.ObjectMapperFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.NestedServletException;

import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

/**
 * @title 统一异常处理
 * @description 统一处理项目内的异常
 * @author Echo_Ye
 * @date 2020/8/24 10:42
 * @email echo_yezi@qq.com
 */
@Slf4j
@ControllerAdvice
public class BaseExceptionHandler {
    /**
     * 统一异常处理，仅处理NestedServletException
     */
    @ExceptionHandler({NestedServletException.class})
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public ModelAndView servletException(HttpServletRequest request, HttpServletResponse response, Exception exception) {
        ModelAndView mav = new ModelAndView();
        ReturnMsg message = ReturnMsg.FAIL;
        out(response, message);
        return mav;
    }

    /**
     * 统一异常处理
     */
    @ExceptionHandler({Exception.class})
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public ReturnMsg processException(HttpServletRequest request, HttpServletResponse response, Exception exception) {
        // 打印异常信息至控制台，开始处理异常
        log.error("异常统一处理-Exception:" + exception.getLocalizedMessage(), exception);
        //异常默认为是操作失败
        ReturnMsg message = ReturnMsg.FAIL;
        // 检查异常的类型
        if (exception instanceof NestedServletException) {
            // 异步请求错误，已处理
        } else if (exception instanceof BaseException) {
            // 自定义类型的异常，转换为自定义异常
            message = ((BaseException) exception).asReturnMsg();
        } else {
            // 非自定义类型异常，打印错误信息至日志，封装ReturnMsg对象
            log.error(request.getRequestURI(), exception);
            message = ReturnMsg.FAIL;
        }
        //返回消息体
        return message;
    }

    /**
     * response 输出JSON
     */
    public static void out(ServletResponse response, ReturnMsg returnMsg) {
        PrintWriter out = null;
        try {
            response.setContentType("application/json;charset=utf-8");
            out = response.getWriter();
            out.println(ObjectMapperFactory.getInstance().writeValueAsString(returnMsg));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != out) {
                out.flush();
                out.close();
            }
        }
    }
}
